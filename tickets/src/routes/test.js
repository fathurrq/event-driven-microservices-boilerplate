const getAccountData = async (token) => {
    const BASE_URL = '/api/account'
    try {
        const response = await fetch(BASE_URL, {
            method: 'GET',
            headers: {
                'Authorization' : `Bearer ${token}`,
                'Content-Type' : 'application/json'
            }
        })
        if (!response.ok) throw new Error(`Error: ${response.status} ${response.statusText}`)





        
                    const data = await response.json()
        return data
    }
          catch(err) {
                throw new Error(`Failed to fetch account data : ${err.message}`)
    }
}